import pandas as pd
import numpy as np

from .utils import input_int
from .board import Board
from .move import Move


class Game:
    """
    A Game takes place on a Board.
    """

    def __init__(self):
        gameOver = False
        gameWon = False
        gameLost = False
        minesCleared = 0
        self.board = Board()

    def move(self):
        the_move = Move()