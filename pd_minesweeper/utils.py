def input_int(prompt_string='input a value', max_val=15):
    valid = False
    while not valid:
        print(prompt_string)
        value = input()
        try:
            value = int(value)
            if value > 0 and value <= max_val:
                return value
        except:
            pass
