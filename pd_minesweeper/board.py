import pandas as pd
import numpy as np

from .utils import input_int


class Board:
    """
    The board is the playing field where the game takes place. It is a rectangular (square) grid of nxm (nxn) cells,
    """

    def __init__(self):
        self.nrows = input_int(prompt_string='number of rows?')
        self.ncols = input_int(prompt_string='number of cols?')
        self.dims = (self.nrows, self.ncols)
        self.ncells = self.dims[0] * self.dims[1]
        self.nmines = input_int(prompt_string='number of mines?', max_val=6)
        self.cleared_mines = 0
        self.cleared_cells = 0
        self.build_gameboard()
        self.build_minefield()

    def build_gameboard(self):
        """Build a dataframe representing the playing board"""
        self.df_gameboard = pd.DataFrame(np.zeros(self.dims))

    def build_minefield(self):
        """Build a dataframe containing mines randomly distributed on the board"""
        arr_mines = np.random.choice(
            [1, 0],
            size=self.ncells,
            p=[self.nmines / self.ncells, (self.ncells - self.nmines) / self.ncells]
        )
        self.df_minefield = pd.DataFrame(arr_mines.reshape(self.dims))

    def get_cell_neighborhood(self, df, cell):
        """Get the 3x3 kernel neighborhood of this cell"""
        i = cell[0]
        j = cell[1]
        min_row = max(0, i - 1)
        max_row = min(i + 2, dims[0])
        min_col = max(0, j - 1)
        max_col = min(j + 2, dims[1])
        df_neighborhood = df.iloc[min_row:max_row, min_col:max_col]
        return df_neighborhood

    def neighborhood_minecount(self, cell):
        """Count the number of mines in this neighborhood"""
        df = self.get_cell_neighborhood(self.df_minefield, cell)
        minecount = df.values.sum()

    def cell_ismine(self, cell):
        """Determine if the chosen cell is a mine"""
        if df_minefield.iat[cell] == 1:
            return True
        return False

    def clear_neighborhood(self, cell):
        """Clear this cell and all of its neighbors"""

