import pandas as pd
import numpy as np
from .utils import input_int


class Move:
    """A move is a single iteration of the game.
    User is prompted to select a cell.
    If the cell is a mine, the game is over and the player loses.
    If the cell is not a mine, the neighborhood is cleared and the game continues.
    The game ends when all mines or all cells have been cleared or when the user selects a mine.
    """

    def __init__(self, game):
        self.cell_x = input_int(prompt_string='Cell x?') - 1  # zero-index
        self.cell_y = input_int(prompt_string='Cell y?') - 1  # zero-index
        self.cell = (self.cell_x, self.cell_y)
